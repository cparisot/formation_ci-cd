FROM node:10.6.0-alpine

ENV SERVER_PORT=
ENV SERVER_DOMAIN_NAME=
ENV ENV_NAME=

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# Building code for production
RUN npm install --only=production

# Bundle app source
COPY . .

EXPOSE 5000
CMD [ "npm", "start" ]
