import Hapi from 'hapi'
import debug from 'debug'
import routes from './routes'

const log = debug('app')

export default class Server {
  constructor(config) {
    const options = {
      host: config.server.host,
      port: config.server.port,
      router: {
        stripTrailingSlash: true
      }
    }
    this.server = new Hapi.Server(options)
    routes.map((route) => this.server.route(route))
  }

  async start() {
    log('Starting Hapi server…')
    const server = this.server

    try {
      await server.start()
      console.log(`Server started at: ${server.info.uri}`) // eslint-disable-line no-console
    } catch (err) {
      console.error('Can’t start server: ' + err) // eslint-disable-line no-console
      process.exit(1)
    }
  }

  async stop() {
    log('Stopping server…')
    this.server.stop({ timeout: 10000 }).then(function (err) {
      log('Hapi server stopped.')
      process.exit((err) ? 1 : 0)
    })
  }
}
