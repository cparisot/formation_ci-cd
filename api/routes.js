import debug from 'debug'
import config from '../config'
const log = debug('app')

const routes = [
  {
    method: 'GET',
    path: '/',
    config: {
      cors: true
    },
    handler: () => {
      log('Hello world route called')
      return 'environment : ' + config.env
    }
  },
  {
    method: 'GET',
    path: '/ping',
    config: {
      cors: true
    },
    handler: () => {
      log('Ping route called')
      return 'pong'
    }
  },
  {
    method: 'GET',
    path: '/healthcheck',
    config: {
      cors: true
    },
    handler: async () => {
      return 'Application is up and running'
    }
  }
]

export default routes
