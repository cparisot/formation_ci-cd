import Server from './api/server'
import config from './config'

const server = new Server(config)
server.start()
process.on('SIGINT', function () {
  server.stop()
})