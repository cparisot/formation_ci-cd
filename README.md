#TP

## Exercice 1

Yearh c’est le début du projet dans une toute nouvelle équipe !

On est au sprint 0, l’équipe a choisi de mettre en place un gitlab comme gestionnaire de source.

 l’équipe n’est pas encore bien sensibilisée à l’utilisation des tests, il leur arrive d’oublier de les lancer avant de push sur le master…

Ah… Si seulement le linter et les tests étaient lancés automatiquement …
 

Le projet est encore petit mais quand il grossira et qu’on ira en prod ce sera la catastrophe, si on casse quelquechose et qu’on ne l’a pas vu : Indisponibilité du site => perte d’argent => client pas content

Vous proposez alors de mettre une CI en place ! Et vous avez justement entendu parlé de GitlabCI.

Vous clonez le repo à l’adresse suivante : https://gitlab.com/cparisot/formation_ci-cd


Et c’est parti ! Mettons en place notre CI. A chaque fois qu’un développement sera fait et poussé sur n’importe quelle branche, on souhaite pouvoir lancer un linter et des tests.

Les devs vous ont laissé le README suivant :

##############################################

### README DEV : Portal - Back-end

### Usage
Run `npm install`

Copy-paste the `.env.example` file, rename it `.env.local` and fill it with your own environment variables.
To run tests, do the same but name it `.env.test`

Then run `npm start`.

### Env variables

SERVER_PORT : Port to use for this API (example : 3000)
SERVER_DOMAIN_NAME : Domain name of the host of this API (example : localhost)
ENV_NAME : current environment

### Debug

To see the debug logs, you must indicate the debug namespace when launching the app, as such :
`DEBUG=app npm start`

##############################################

## Exercice 2

Cool, on a notre CI en place. Maintenant vous voulez pouvoir déployer votre application sur un environnement de qualification. Une app qui tourne en local et où le PO ne peut pas recetter, c’est pas vraiment top… Hop vous vous mettez au boulot ! 

 Vous souhaitez pouvoir packager votre application dans une image docker, plus qu’à la faire tourner sur l’environnement cible après.


Vous voyez un magnifique Dockerfile développé par l’équipe, vous allez pouvoir vous en servir pour packager l'application !

On rajoute une nouvelle étape où l’on package notre application dans une image Docker et où l’on pousse cette image sur la registry associée à notre projet gitlab. Pour vous aider deux variables ont été renseignées dans les variables CI/CD sur gitlab CI_GITLAB_REGISTRY_USER et CI_GITLAB_REGISTRY_PWD.

 On ne veut générer cette image qu’au moment de push sur master. Les devs ne font leurs tests sur l’environnement local et rien ne sert d’avoir des images qui ne correspondent pas à ce que l’on veut délivrer.

## Exercice 3

Notre application est maintenant packagée, il ne nous reste plus qu’à la déployer sur l’environnement de qualification.


Un dossier ansible est disponible dans le projet ! Il contient un playbook app-deploy.yml pour configurer notre machine de production et déployer notre application dessus.

Attention des secrets ont été configuré dans l’ansile-vault !

 Pour vous aider deux variables ont été renseignées dans les variables CI/CD sur gitlab ANSIBLE_HOST_KEY_CHECKING, SSH_KEY et ANSIBLE_VAULT_ID

Rajoutons une nouvelle étape à notre CI pour pousser sur l’environnement de qualification.


—

Oh mais en regardant dans le dossier ansible, on peut voir qu’il y a aussi les variables et l’inventaire pour déployer en production. Et oui, une machine a aussi été créée pour la prod.

Rajoutons une nouvelle étape pour pousser en production.

On ne souhaite pas lancer cette étape à chaque fois que l’on pousse sur master, seulement quand le PO a recetté et qu’il a dit ok, on souhaite donc une action manuelle.   
