import Server from '../api/server'
import config from '../config'
import request from 'supertest'

const server = new Server(config)

describe('GET /ping', () => {
  it('should respond 200 status code', () => {
    return request(server.server.listener).get('/ping').expect(200)
  })
})

describe('GET /healthcheck', () => {
  it('should respond 200 status code', () => {
    return request(server.server.listener).get('/healthcheck').expect(200)
  })
})

describe('GET /', () => {
  it('should respond 200 status code', () => {
    return request(server.server.listener).get('/').expect(200)
  })
})