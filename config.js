import {forEach, set} from 'lodash'

const dotenvPath = process.env.NODE_ENV === 'test' ? '.env.test' : '.env.local'
require('dotenv').config({path: dotenvPath})

const envVars = {
  'server.port': 'SERVER_PORT',
  'server.host': 'SERVER_DOMAIN_NAME',
  'env': 'ENV_NAME'
}


const config = {}

// check if environment variable is set
forEach(envVars, (envVar, key) => {
  const value = process.env[envVar]
  if (!value) throw new Error(`${envVar} variable is not set`)
  set(config, key, value)
})

export default config
